<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project aedev.aedev V0.3.15 -->
<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project aedev.tpl_namespace_root V0.3.12 -->
# setup_hook 0.3.6

[![GitLab develop](https://img.shields.io/gitlab/pipeline/aedev-group/aedev_setup_hook/develop?logo=python)](
    https://gitlab.com/aedev-group/aedev_setup_hook)
[![LatestPyPIrelease](
    https://img.shields.io/gitlab/pipeline/aedev-group/aedev_setup_hook/release0.3.5?logo=python)](
    https://gitlab.com/aedev-group/aedev_setup_hook/-/tree/release0.3.5)
[![PyPIVersions](https://img.shields.io/pypi/v/aedev_setup_hook)](
    https://pypi.org/project/aedev-setup-hook/#history)

>aedev namespace module portion setup_hook: individually configurable setup hook.

[![Coverage](https://aedev-group.gitlab.io/aedev_setup_hook/coverage.svg)](
    https://aedev-group.gitlab.io/aedev_setup_hook/coverage/index.html)
[![MyPyPrecision](https://aedev-group.gitlab.io/aedev_setup_hook/mypy.svg)](
    https://aedev-group.gitlab.io/aedev_setup_hook/lineprecision.txt)
[![PyLintScore](https://aedev-group.gitlab.io/aedev_setup_hook/pylint.svg)](
    https://aedev-group.gitlab.io/aedev_setup_hook/pylint.log)

[![PyPIImplementation](https://img.shields.io/pypi/implementation/aedev_setup_hook)](
    https://gitlab.com/aedev-group/aedev_setup_hook/)
[![PyPIPyVersions](https://img.shields.io/pypi/pyversions/aedev_setup_hook)](
    https://gitlab.com/aedev-group/aedev_setup_hook/)
[![PyPIWheel](https://img.shields.io/pypi/wheel/aedev_setup_hook)](
    https://gitlab.com/aedev-group/aedev_setup_hook/)
[![PyPIFormat](https://img.shields.io/pypi/format/aedev_setup_hook)](
    https://pypi.org/project/aedev-setup-hook/)
[![PyPILicense](https://img.shields.io/pypi/l/aedev_setup_hook)](
    https://gitlab.com/aedev-group/aedev_setup_hook/-/blob/develop/LICENSE.md)
[![PyPIStatus](https://img.shields.io/pypi/status/aedev_setup_hook)](
    https://libraries.io/pypi/aedev-setup-hook)
[![PyPIDownloads](https://img.shields.io/pypi/dm/aedev_setup_hook)](
    https://pypi.org/project/aedev-setup-hook/#files)


## installation


execute the following command to install the
aedev.setup_hook module
in the currently active virtual environment:
 
```shell script
pip install aedev-setup-hook
```

if you want to contribute to this portion then first fork
[the aedev_setup_hook repository at GitLab](
https://gitlab.com/aedev-group/aedev_setup_hook "aedev.setup_hook code repository").
after that pull it to your machine and finally execute the
following command in the root folder of this repository
(aedev_setup_hook):

```shell script
pip install -e .[dev]
```

the last command will install this module portion, along with the tools you need
to develop and run tests or to extend the portion documentation. to contribute only to the unit tests or to the
documentation of this portion, replace the setup extras key `dev` in the above command with `tests` or `docs`
respectively.

more detailed explanations on how to contribute to this project
[are available here](
https://gitlab.com/aedev-group/aedev_setup_hook/-/blob/develop/CONTRIBUTING.rst)


## namespace portion documentation

information on the features and usage of this portion are available at
[ReadTheDocs](
https://aedev.readthedocs.io/en/latest/_autosummary/aedev.setup_hook.html
"aedev_setup_hook documentation").
